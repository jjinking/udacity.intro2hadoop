#!/usr/bin/python

import sys

w_old = ''
nodes = []

for line in sys.stdin:
    data_mapped = line.strip().split("\t")
    if len(data_mapped) != 2:
        # Something has gone wrong. Skip this line.
        continue

    w, node = data_mapped

    if w_old and w_old != w:
        print "{0}\t{1}".format(w_old, ', '.join(sorted(nodes)))
        w_old = w
        nodes = []

    w_old = w
    nodes.append(node)

if w_old != None:
    print "{0}\t{1}".format(w_old, ', '.join(sorted(nodes)))
