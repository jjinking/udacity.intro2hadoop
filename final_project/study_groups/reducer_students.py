#!/usr/bin/python

import csv
import sys

node_old = ''
students = []

reader = csv.reader(sys.stdin, delimiter='\t')
for row in reader:

    if len(row) != 2:
        # Something has gone wrong. Skip this line.
        continue

    try:
        node_curr = row[0]
        student_curr = row[1]
    except ValueError:
        continue

    if node_old and node_old != node_curr:
        print "{0}\t{1}".format(node_old, ', '.join(students))
        students = []

    node_old = node_curr
    students.append(student_curr)

if node_old != None:
    print "{0}\t{1}".format(node_old, ', '.join(students))
