#!/usr/bin/python

import csv
import sys
from collections import defaultdict

node_old = ''
body_lengths = []
question_length = defaultdict(int)

reader = csv.reader(sys.stdin, delimiter='\t')
for row in reader:

    if len(row) != 3:
        # Something has gone wrong. Skip this line.
        continue

    node_curr = row[0]
    node_type = row[1]
    body_length_curr = int(row[2])
    
    if node_type == "question":
        question_length[node_curr] = body_length_curr
        continue

    # answer -------------------------

    if node_old and node_old != node_curr:
        if node_old in question_length:
            print "{0}\t{1}\t{2}".format(node_old, question_length[node_old], float(sum(body_lengths))/len(body_lengths))
        body_lengths = []

    node_old = node_curr
    body_lengths.append(body_length_curr)


if node_old != None:
    if node_old in question_length:
        print "{0}\t{1}".format(node_old, float(sum(body_lengths))/len(body_lengths))
