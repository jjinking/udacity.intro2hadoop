#!/usr/bin/python

import csv
import sys
from datetime import datetime

reader = csv.reader(sys.stdin, delimiter='\t')
for row in reader:

    if len(row) != 6:
        continue

    date = datetime.strptime(row[0], "%Y-%m-%d").weekday()
    amnt = row[4]
    
    try:
        print "{0}\t{1}".format(date, float(amnt))
    except ValueError:
        continue

