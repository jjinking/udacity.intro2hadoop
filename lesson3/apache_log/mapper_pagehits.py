#!/usr/bin/python

import re
import sys

for line in sys.stdin:

    data = line.strip().split()
    page, numhits = data
    print "{0}\t{1}".format(page, numhits)

