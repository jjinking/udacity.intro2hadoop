#!/usr/bin/python

import sys

w_old = ''
wc = 0

for line in sys.stdin:
    data_mapped = line.strip().split("\t")
    if len(data_mapped) != 2:
        # Something has gone wrong. Skip this line.
        continue

    w, node = data_mapped

    if w_old and w_old != w:
        print "{0}\t{1}".format(w_old, wc)
        w_old = w
        wc = 0

    w_old = w
    wc += 1

if w_old != None:
    print "{0}\t{1}".format(w_old, wc)

