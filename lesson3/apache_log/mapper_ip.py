#!/usr/bin/python

import re
import sys

'''
10.223.157.186 - - [15/Jul/2009:14:58:59 -0700] "GET / HTTP/1.1" 403 202
10.223.157.186 - - [15/Jul/2009:14:58:59 -0700] "GET /favicon.ico HTTP/1.1" 404 209
10.223.157.186 - - [15/Jul/2009:15:50:35 -0700] "GET / HTTP/1.1" 200 9157
10.223.157.186 - - [15/Jul/2009:15:50:35 -0700] "GET /assets/js/lowpro.js HTTP/1.1" 200 10469
10.223.157.186 - - [15/Jul/2009:15:50:35 -0700] "GET /assets/css/reset.css HTTP/1.1" 200 1014
10.223.157.186 - - [15/Jul/2009:15:50:35 -0700] "GET /assets/css/960.css HTTP/1.1" 200 6206
10.223.157.186 - - [15/Jul/2009:15:50:35 -0700] "GET /assets/css/the-associates.css HTTP/1.1" 200 15779
10.223.157.186 - - [15/Jul/2009:15:50:35 -0700] "GET /assets/js/the-associates.js HTTP/1.1" 200 4492
'''

regex = r'([0-9\.]+) \- \- \[(.*)\] \"(.*)\" (.+) (.+)'
logpat = re.compile(regex)

for line in sys.stdin:

    data = line.strip()
    result = logpat.match(data)
    if result:
        ip, time, request, status, bsize = result.groups()

        request_fields = request.split()
        if len(request_fields) != 3:
            continue
        req_type, doc, vers = request_fields
        print "{0}\t{1}".format(ip, 1)

