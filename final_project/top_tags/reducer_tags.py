#!/usr/bin/python

import csv
import sys
from Queue import PriorityQueue

key_old = ''
total = 0
N = 10
pq = PriorityQueue(maxsize=N)

reader = csv.reader(sys.stdin, delimiter='\t')
for row in reader:

    if len(row) != 2:
        # Something has gone wrong. Skip this line.
        continue

    try:
        key_curr = row[0]
        val_curr = float(row[1])
    except ValueError:
        continue

    if key_old and key_old != key_curr:
        if not pq.full():
            pq.put((total, key_old))
        else:
            lowest = pq.get()
            if total > lowest[0]:
                pq.put((total, key_old))
            else:
                pq.put(lowest)
        total = 0

    key_old = key_curr
    total += val_curr

if key_old != None:
    lowest = pq.get()
    if total > lowest[0]:
        pq.put((total, key_old))
    else:
        pq.put(lowest)

results = []
while not pq.empty():
    el = pq.get()
    results.append(el)

for el in sorted(results, key=lambda x: x[0], reverse=True):
    print "{0}\t{1}".format(el[1], el[0])
