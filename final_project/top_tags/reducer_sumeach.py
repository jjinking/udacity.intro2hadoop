#!/usr/bin/python

import csv
import sys

key_old = ''
total = 0

reader = csv.reader(sys.stdin, delimiter='\t')
for row in reader:

    if len(row) != 2:
        # Something has gone wrong. Skip this line.
        continue

    try:
        key_curr = row[0]
        val_curr = float(row[1])
    except ValueError:
        continue

    if key_old and key_old != key_curr:
        print "{0}\t{1}".format(key_old, total)        
        total = 0

    key_old = key_curr
    total += val_curr

if key_old != None:
    print "{0}\t{1}".format(key_old, total)
