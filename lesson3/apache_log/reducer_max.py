#!/usr/bin/python

import sys

maxkey = ''
maxval = -1

for line in sys.stdin:
    data_mapped = line.strip().split()
    thisKey, thisVal = data_mapped
    thisVal = float(thisVal)

    if thisVal > maxval:
        maxval = thisVal
        maxkey = thisKey

print "{0}\t{1}".format(maxkey, maxval)
