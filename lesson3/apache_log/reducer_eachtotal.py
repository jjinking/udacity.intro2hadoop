#!/usr/bin/python

import sys

totalcount = 0
oldKey = None

for line in sys.stdin:
    data_mapped = line.strip().split("\t")
    if len(data_mapped) != 2:
        # Something has gone wrong. Skip this line.
        continue

    thisKey, thisVal = data_mapped

    if oldKey and oldKey != thisKey:
        print oldKey, "\t", totalcount
        oldKey = thisKey;
        totalcount = 0

    oldKey = thisKey
    totalcount += 1

if oldKey != None:
    print oldKey, "\t", totalcount

