#!/usr/bin/env python

import csv
import sys
from collections import defaultdict

old_userid = ''
hr2num = defaultdict(int)

reader = csv.reader(sys.stdin, delimiter='\t')
for row in reader:

    if len(row) != 2:
        # Something has gone wrong. Skip this line.
        continue

    current_userid = row[0]
    current_hr = row[1]

    if old_userid and old_userid != current_userid:
        # Find the maximum time
        sorted_hrs = sorted(hr2num.keys(), key=lambda x: hr2num[x], reverse=True)
        maximum_times = hr2num[sorted_hrs[0]]
        for _hr in sorted_hrs:
            if hr2num[_hr] == maximum_times:
                print "{0}\t{1}".format(old_userid, _hr)
        hr2num = defaultdict(int)

    old_userid = current_userid
    hr2num[current_hr] += 1

if old_userid != None:
    # Find the maximum time
    sorted_hrs = sorted(hr2num.keys(), key=lambda x: hr2num[x], reverse=True)
    maximum_times = hr2num[sorted_hrs[0]]
    for _hr in sorted_hrs:
        if hr2num[_hr] == maximum_times:
            print "{0}\t{1}".format(old_userid, _hr)
