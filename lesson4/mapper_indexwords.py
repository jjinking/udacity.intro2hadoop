#!/usr/bin/python
'''
columns (19 total):

"id"
"title"
"tagnames"
"author_id"
"body"
"node_type"
"parent_id"
"abs_parent_id"
"added_at"
"score"
"state_string"
"last_edited_id"
"last_activity_by_id"
"last_activity_at"
"active_revision_id"
"extra"
"extra_ref_id"
"extra_count"
"marked"
'''

import csv
import re
import sys

reader = csv.reader(sys.stdin, delimiter='\t')
for row in reader:

    if row[0] == "id" and row[-1] == "marked":
        continue

    nid = row[0]
    body = row[4]

    for w in (w.lower() for w in re.split('\W+', body)):
        if w:
            print "{0}\t{1}".format(w, nid)

