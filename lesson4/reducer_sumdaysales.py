#!/usr/bin/python

import csv
import sys

day_old = ''
total = 0
counter = 0

reader = csv.reader(sys.stdin, delimiter='\t')
for row in reader:

    if len(row) != 2:
        # Something has gone wrong. Skip this line.
        continue

    try:
        day = row[0]
        sale = float(row[1])
    except ValueError:
        continue

    if day_old and day_old != day:
        print "{0}\t{1}".format(day_old, total)
        day_old = day
        counter = 0
        total = 0

    day_old = day
    counter += 1
    total += sale

if day_old != None:
    print "{0}\t{1}".format(day_old, total)
